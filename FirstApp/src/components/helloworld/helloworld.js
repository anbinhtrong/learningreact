import React, { Component } from 'react';

class HelloWorld extends Component{

    constructor(props){
        super(props);
        let firstName = this.props.name.split(" ")[1];
        this.state = {
            name: firstName
        }
    }

    render(){
        return <h1>Hello, {this.state.name}</h1>
    }
}

export default HelloWorld;