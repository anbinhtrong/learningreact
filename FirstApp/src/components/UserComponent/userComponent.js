import React, { Component } from 'react';
//import PropTypes from "prop­types";

class UserComponent extends Component{
    render(){
        return (<span>
            The user's ID is {this.props.userId}
            <Person></Person>
            </span>)
    }
}

const Person = () => <div>Michael Sandel</div>

// // defined at the bottom of MyComponent
// UserComponent.propTypes = {
//     someObject: React.PropTypes.object,
//     userID: React.PropTypes.number.isRequired,
//     title: React.PropTypes.string
// };
// UserComponent.defaultProps = {
//     someObject: {},
//     title: 'My Default Title'
// }

export default UserComponent;