import React from 'react';

const FirstComponent = props => (
    <div>
        Hello , {props.name}! I'm a FirstComponent
    </div>
);

export default FirstComponent;