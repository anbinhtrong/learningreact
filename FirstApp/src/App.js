import React from 'react';
import logo from './logo.svg';
import './App.css';
import HelloWorld from './components/helloworld/helloworld';
import FirstComponent from './components/functional-component/first-component';
import UserComponent from './components/UserComponent/userComponent';

function App() {
  return (
    <div className="App">
      <header className="App-header">
        <img src={logo} className="App-logo" alt="logo" />
        <HelloWorld name="A Lin"></HelloWorld>
        <a
          className="App-link"
          href="https://reactjs.org"  
          target="_blank"
          rel="noopener noreferrer"
        >
          Learn React
        </a>
        <FirstComponent name="A Lin"></FirstComponent>
        <UserComponent userId="2103"></UserComponent>
      </header>
    </div>
  );
}

export default App;
