import React from "react";
import { BrowserRouter, Route, Link } from "react-router-dom";
import "./App.css";

import HomePage from "./components/HomePage/HomePage";
import AboutPage from "./Pages/About/About";

function App() {
  return (
    <BrowserRouter>
      <div className="row">
        <div className="col-lg-12">
          <ul>
            <li>
              <Link to="/">Home</Link>
            </li>
            <li>
              <Link to="/about">About</Link>
            </li>
          </ul>
        </div>

        <div className="main-route-place">
          <div className="col-lg-12">
            <Route exact path="/" component={HomePage} />
            <Route exact path="/about" component={AboutPage} />
            {/* <Route path="/about" component={About} />
            <Route path="/topics" component={Topics} /> */}
          </div>
        </div>
      </div>
    </BrowserRouter>
  );
}

export default App;
