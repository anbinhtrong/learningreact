import React, { Component } from "react";

class AboutPage extends Component {
  render() {
    return (
      <div>
        <p>
          <b>Đông Châu liệt quốc chí</b> (
          <a href="/wiki/Ch%E1%BB%AF_H%C3%A1n" title="Chữ Hán">
            chữ Hán
          </a>
          : 東周列國志) là tiểu thuyết lịch sử gồm 108 hồi được Sái Nguyên Phóng
          thời{" "}
          <a href="/wiki/Nh%C3%A0_Thanh" title="Nhà Thanh">
            Thanh
          </a>{" "}
          cải biên từ <i>Tân liệt quốc chí</i> khoảng trên 700.000 chữ của{" "}
          <a href="/wiki/Ph%C3%B9ng_M%E1%BB%99ng_Long" title="Phùng Mộng Long">
            Phùng Mộng Long
          </a>{" "}
          thời{" "}
          <a href="/wiki/Nh%C3%A0_Minh" title="Nhà Minh">
            Minh
          </a>{" "}
          mạt. Bản thân Tân liệt quốc chí lại được cải biên và viết thêm từ bộ{" "}
          <i>Liệt quốc chí truyện</i> khoảng 280.000 chữ do Dư Thiệu Ngư viết ra
          khoảng{" "}
          <a href="/wiki/Ni%C3%AAn_hi%E1%BB%87u" title="Niên hiệu">
            niên hiệu
          </a>{" "}
          <a href="/wiki/Minh_Th%E1%BA%BF_T%C3%B4ng" title="Minh Thế Tông">
            Gia Tĩnh
          </a>
          .
        </p>
        <p>
          Đông Châu liệt quốc đề cập đến thời kỳ{" "}
          <a href="/wiki/L%E1%BB%8Bch_s%E1%BB%AD" title="Lịch sử">
            lịch sử
          </a>{" "}
          rất dài khoảng hơn 500 năm (770 TCN - 221 TCN) của{" "}
          <a href="/wiki/Trung_Qu%E1%BB%91c" title="Trung Quốc">
            Trung Quốc
          </a>
          , bắt đầu từ đời Tuyên vương{" "}
          <a href="/wiki/Nh%C3%A0_Chu" title="Nhà Châu">
            nhà Châu
          </a>{" "}
          cho đến khi{" "}
          <a
            href="/wiki/T%E1%BA%A7n_Th%E1%BB%A7y_Ho%C3%A0ng"
            title="Tần Thủy Hoàng"
          >
            Tần Thủy Hoàng
          </a>{" "}
          thống nhất Trung Hoa. Sử sách cũng gọi thời kỳ ấy là đời{" "}
          <a
            href="/wiki/%C4%90%C3%B4ng_Chu"
            className="mw-redirect"
            title="Đông Châu"
          >
            Đông Châu
          </a>
          , được chia làm hai giai đoạn là{" "}
          <a href="/wiki/Xu%C3%A2n_Thu" title="Xuân Thu">
            Xuân Thu
          </a>{" "}
          và{" "}
          <a href="/wiki/Chi%E1%BA%BFn_Qu%E1%BB%91c" title="Chiến Quốc">
            Chiến Quốc
          </a>
          . Trong lịch sử Trung Hoa đây là thời kỳ quá độ từ chế độ{" "}
          <a href="/wiki/Phong_ki%E1%BA%BFn" title="Phong kiến">
            phong kiến
          </a>{" "}
          đến{" "}
          <a
            href="/wiki/Qu%C3%A2n_ch%E1%BB%A7_chuy%C3%AAn_ch%E1%BA%BF"
            title="Quân chủ chuyên chế"
          >
            tập quyền
          </a>
          . Đông Châu liệt quốc là bộ sách rất hay, trong truyện không những đề
          cập đến các mốc lịch sử rất dài đồng thời cũng đề cập, mô tả rất nhiều
          nhân vật nổi tiếng của lịch sử Trung Hoa, từ các bậc anh hùng như{" "}
          <a
            href="/wiki/T%C3%ADn_L%C4%83ng_Qu%C3%A2n"
            className="mw-redirect"
            title="Tín Lăng Quân"
          >
            Tín Lăng quân
          </a>
          ,{" "}
          <a
            href="/wiki/L%E1%BA%A1n_T%C6%B0%C6%A1ng_Nh%C6%B0"
            title="Lạn Tương Như"
          >
            Lạn Tương Như
          </a>
          ,{" "}
          <a href="/wiki/Ng%C3%B4_Kh%E1%BB%9Fi" title="Ngô Khởi">
            Ngô Khởi
          </a>
          ,{" "}
          <a href="/wiki/Ng%C5%A9_T%E1%BB%AD_T%C6%B0" title="Ngũ Tử Tư">
            Ngũ Tử Tư
          </a>{" "}
          đến các nhà quân sự lỗi lạc như{" "}
          <a
            href="/wiki/T%C3%B4n_T%E1%BB%AD"
            className="mw-redirect"
            title="Tôn Tử"
          >
            Tôn Tử
          </a>
          ,{" "}
          <a href="/wiki/Ph%E1%BA%A1m_L%C3%A3i" title="Phạm Lãi">
            Phạm Lãi
          </a>
          ,{" "}
          <a href="/wiki/T%C3%B4n_T%E1%BA%ABn" title="Tôn Tẫn">
            Tôn Tẫn
          </a>{" "}
          và các nhà chính trị-tư tưởng lớn như{" "}
          <a href="/wiki/Kh%E1%BB%95ng_T%E1%BB%AD" title="Khổng Tử">
            Khổng Tử
          </a>
          ,{" "}
          <a href="/wiki/Qu%E1%BA%A3n_Tr%E1%BB%8Dng" title="Quản Trọng">
            Quản Trọng
          </a>
          .
        </p>
      </div>
    );
  }
}

export default AboutPage;
