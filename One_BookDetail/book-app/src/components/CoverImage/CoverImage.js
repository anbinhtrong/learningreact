import React from 'react';

function CoverImage(){
    return CoverImageFunc();
}
const CoverImageFunc = () => (
    <div>
        <img src="https://cdn0.fahasa.com/media/catalog/product/cache/1/image/9df78eab33525d08d6e5fb8d27136e95/8/1/81ddwazxtrl.jpg" alt='Cover Image'
        width='120' className="img-thumbnail"/>
    </div>

);

export default CoverImage;