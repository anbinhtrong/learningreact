import React, { Component } from 'react';
import CoverImage from '../CoverImage/CoverImage';
import Title from '../Title/Title';
import Author from '../Author/Author';
import styles from '../book/book.module.css';
import classNames from 'classnames'


class Book extends Component{
    render(){
        var btnClass = classNames(
            'rounded float-left',
            styles.book
          );
        return <article className={btnClass}>
            <CoverImage></CoverImage>
            <Title></Title>
            <Author></Author>
        </article>
    }
}

export default Book;